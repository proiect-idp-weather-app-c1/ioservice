import express from 'express';
import bodyParser from 'body-parser';
import * as config from './config/config';
import { Database } from './database/database';

const app = express();
const port = config.PORT;

// get instance of Database
let db = Database.getInstance();

db.connect()
.then(_ => {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    /**
     * {
     *     "collection": "Tari",
     *     "filters": {
     *         object
     *      }
     * }
     * 
     * */ 

    // ROUTES FOR DB
    app.get('/db/getDocuments',
        async (request: express.Request, response: express.Response) => {
            try {
                let result = await db.getDocuments(
                    request.query.collection as string,
                    JSON.parse(request.query.filters as string)
                );

                response.status(200);
                response.send(result);
            } catch (err) {
                response.sendStatus(500);
            }
        }
    );

    app.post('/db/insertDocument',
        async (request: express.Request, response: express.Response) => {
            try {
                let result = await db.insertDocument(
                    request.body.collection as string,
                    request.body.document
                );

                response.status(200);
                response.send(result);
            } catch (err) {
                response.sendStatus(500);
            }
        }
    );

    app.put('/db/updateDocument',
        async (request: express.Request, response: express.Response) => {
            try {
                let result = await db.updateDocument(
                    request.body.collection as string,
                    request.body.filters,
                    request.body.document
                );

                response.status(200);
                response.send(result);
            } catch (err) {
                response.sendStatus(500);
            }
        }
    );

    app.delete('/db/deleteDocument',
        async (request: express.Request, response: express.Response) => {
            let result = await db.deleteDocument(
                request.query.collection as string,
                JSON.parse(request.query.filters as string)
            );

            response.status(200);
            response.send(result);
        }
    );

    app.get('/db/getMaxId',
        async (request: express.Request, response: express.Response) => {
            try {
                let result = await db.getMaxId(
                    request.query.collection as string
                );

                response.status(200);
                response.send(result);
            } catch (err) {
                response.sendStatus(500);
            }
        }
    );

    // listen on a specific port
    app.listen(port, () => {
        console.log('Application listening on port ' + port + " !");
    });
})
.catch(err => {
    console.log(err);
});