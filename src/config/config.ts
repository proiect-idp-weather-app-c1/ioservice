// MONGO CONFIG
export const MONGO_URI: string = "mongodb://mongo:27017/db";
export const MONGO_DATABASE: string = "db";
export const MONGO_COUNTRIES_COLLECTION: string = "Tari";
export const MONGO_CITIES_COLLECTION: string = "Orase";
export const MONGO_TEMPERATURES_COLLECTION: string = "Temperaturi";

// APP PORT
export const PORT: number = 8000;