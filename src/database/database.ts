import { MongoClient, Db, MongoServerError } from 'mongodb';
import * as config from '../config/config';

export class Database {
    private client: MongoClient;
    private db: Db;
    private static instance: Database;

    private constructor() {
        this.client = new MongoClient(config.MONGO_URI);
    }

    static getInstance() {
        if (this.instance) {
            return this.instance;
        }

        this.instance = new Database();

        return this.instance;
    }

    public async getDocuments(collectionName: string, filters: Object = {}) {
        let result = {};

        try {
            const cursor = this.db
                    .collection(collectionName)
                    .find(filters);

            result = await cursor.toArray();
        } catch (err) {
            console.error(err);
        } finally {
            return JSON.parse(JSON.stringify(result));
        }
    }

    public async insertDocument(collectionName: string, documentToInsert: Object) {
        let result = {};

        try {
            result = await this.db
                .collection(collectionName)
                .insertOne(documentToInsert);
            
            return JSON.parse(JSON.stringify(result));
        } catch (err: any | MongoServerError) {
            if (err.code === 11000) {
                return "duplicate";
            }
        }
    }

    public async updateDocument(collectionName: string, filters: Object, dataToUpdate: Object) {
        try {
            await this.db
                .collection(collectionName)
                .updateOne(filters, {'$set': dataToUpdate});

        } catch (err: any | MongoServerError) {
            if (err.code === 11000) {
                return "duplicate";
            }
        }
    }

    public async deleteDocument(collectionName: string, filters: Object) {
        try {
            await this.db
                .collection(collectionName)
                .deleteOne(filters);
        } catch (err: any | MongoServerError) {
            console.error(err);
        }
    }

    public async aggregateCollection(collectionName: string, pipelineCommands: Object[]) {
        let result = {};

        try {
            const cursor = this.db
                .collection(collectionName)
                .aggregate(pipelineCommands);

            result = await cursor.toArray();
        } catch (err) {
            console.error(err);
        } finally {
            return JSON.parse(JSON.stringify(result));
        }
    }

    public async getMaxId(collectionName: string) {
        let result = {};

        try {
            const cursor = this.db
                    .collection(collectionName)
                    .find({})
                    .sort({"id": -1})
                    .limit(1);

            result = await cursor.toArray();
        } catch (err) {
            console.error(err);
        } finally {
            return JSON.parse(JSON.stringify(result));
        }
    }

    public async connect() {
        try {
            await this.client.connect();

            this.db = this.client.db(config.MONGO_DATABASE);
        } catch (err) {
            console.error(err);
        }
    }

    public async disconnect() {
        try {
            await this.client.close();
        } catch (err) {
            console.error(err);
        }
    }
    
    public getDb(): Db {
        return this.db;
    }
}